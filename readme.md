# Follow green object
Robot that detects a green object with it's camera and then rotates towards it.


This project was done on a [Waveshare Jetank](https://www.waveshare.com/jetank-ai-kit.htm). 

This [link](https://www.youtube.com/watch?v=iOq1ju59zz4) shows a demonstration of the robot. 

The files ´Motors.h´ and ´Servos.h´ were included from [Jetank-cpp](https://github.com/tellefo92/jetank_cpp), a project I made along with two others to be able to control the robot using c++. 

## Requirements
To run the software requires an assembled Jetank running this [image](https://jetbot.org/master/software_setup/sd_card.html). `opencv` must be installed.
`gcc`, `cmake` should be included with the image.

## Build
```bash
mkdir build
cd build

sudo chmod 666 /dev/ttyTHS1

cmake ..
make
```
## Authors
Ole Kjepso [@olemikole](https://gitlab.com/olemikole)

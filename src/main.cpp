#include <Robot.h>
#include <thread>
#include <chrono>
#include <string>
#include <opencv2/opencv.hpp>

/* Dumb shit to make the camera work */
std::string gstreamer_pipeline (int capture_width, int capture_height, int display_width, int display_height, int framerate, int flip_method) 
{
    return "nvarguscamerasrc ! video/x-raw(memory:NVMM), width=(int)" + std::to_string(capture_width) + ", height=(int)" +
           std::to_string(capture_height) + ", format=(string)NV12, framerate=(fraction)" + std::to_string(framerate) +
           "/1 ! nvvidconv flip-method=" + std::to_string(flip_method) + " ! video/x-raw, width=(int)" + std::to_string(display_width) + ", height=(int)" +
           std::to_string(display_height) + ", format=(string)BGRx ! videoconvert ! video/x-raw, format=(string)BGR ! appsink";
}


struct Point
{
	int x;
	int y;
};

void getGreenCoord(cv::Mat frame, Point& point, bool& processing)
{
		cv::cvtColor(frame,frame,cv::COLOR_BGR2HSV);

		cv::Vec3i lower(30, 80, 105);
		cv::Vec3i upper(50, 255,150);

		cv::Mat mask;
		cv::inRange(frame,lower,upper,mask);

		int count = 0;
		for(int row=0; row < mask.rows; ++row)
		{
			uchar* p = mask.ptr(row);
			for(int col=0; col < mask.cols; ++col)
			{
				if(*p != 0)
				{
					++count;
					point.x += col;
					point.y += row;
				}
				++p;
			}
		}
		if(count > 5)
		{
			point.x = point.x/count;
			point.y = point.y/count;
		}
		else
			point = {-1,-1};
		processing = false;
		
}




int main()
{
	using namespace std::chrono_literals;
	int capture_width = 1280 ;
    int capture_height = 720 ;
    int display_width = 1280 ;
    int display_height = 720 ;
    int framerate = 60 ;
    int flip_method = 0 ;
	std::string pipeline = gstreamer_pipeline(capture_width,
		capture_height,
		display_width,
		display_height,
		framerate,
		flip_method);

	cv::VideoCapture cap;
	cap.open(pipeline,cv::CAP_GSTREAMER);

	Robot robot;
	std::thread t;
	Point green_avg = {-1,-1}; // 
	bool processing=false;
	try
	{
		auto start_time = std::chrono::steady_clock::now();
		std::cout<<"Starting loop"<<std::endl;
		while(true)
		{
			cv::Mat frame;
			bool found = cap.read(frame);

			if(!processing && found)
			{
				if(t.joinable())
					t.join();
			
				robot.MoveCamera(green_avg.x,green_avg.y);
				robot.MoveBody();
				t = std::thread(getGreenCoord,frame, std::ref(green_avg), std::ref(processing));	
			}
			if( std::chrono::steady_clock::now() - start_time > 120s )
			{
				break;
			}
		
		}
	}
	catch(const std::exception& e)
	{
		std::cerr << e.what() << '\n';
	}
	t.join();
}

#include "Robot.h"
#include <thread>
#include <chrono>



Robot::Robot()
{
    using namespace std::chrono_literals;
    if(!servos.isConnected())
    {
        return;
    }
    motors.setSpeed(0.7);
    motors.moveStop();

    for(int i =1;i<6;i++)
    {
        servos.setServoSpeed(i,700);
        std::this_thread::sleep_for(10ms);
    }
    
    servos.setServoAngle(1,0);
    servos.setServoAngle(5,0);
    servos.setServoAngle(3,45);
    servos.setServoAngle(4,-20);
}

void Robot::MoveCamera(int x, int y,int camera_width, int camera_height)
{
    servo_data_x = servos.getServoData(1);
    servo_data_y = servos.getServoData(5);


    int cam_angle_x = servo_data_x.current_angle;
    int cam_speed_x = servo_data_x.current_angle;

    int cam_angle_y = servo_data_y.current_angle;
    int cam_speed_y = servo_data_y.current_angle;

    if(x == -1)
    {
        not_found_count +=1;
        if(not_found_count<10)
            servos.setServoAngle(1,0);
        else{}
            //servos.setServoAngle(1,cam_angle_x);
        return;
    }
    not_found_count = 0;

    int offset_x = x - camera_width/2;
    int offset_y = y - camera_height/2;

    if(offset_x > 50 && cam_angle_x < 90 )
    {
        if(offset_x > 300)
            servos.setServoAngle(1,cam_angle_x  + 30);
        else if(offset_x > 160)
            servos.setServoAngle(1,cam_angle_x + 10);
        else if(offset_x > 100)
            servos.setServoAngle(1,cam_angle_x + 5);
        else
            servos.setServoAngle(1,cam_angle_x + 3);
    }

    else if( offset_x < -50 && cam_angle_x > -90 )
    {
        if(offset_x < -300)
            servos.setServoAngle(1,cam_angle_x  - 30);
        else if(offset_x < -160)
            servos.setServoAngle(1,cam_angle_x - 10);
        else if(offset_x > -100)
            servos.setServoAngle(1,cam_angle_x - 5);
        else
            servos.setServoAngle(1,cam_angle_x - 3);
    }
    else
    {
        servos.setServoAngle(1,cam_angle_x);
    }


    if(offset_y < -60 && cam_angle_y > -30)
	{
		servos.setServoAngle(5,cam_angle_y-5);
	}
	if(offset_y > 60 && cam_angle_y < 30)
	{
		servos.setServoAngle(5,cam_angle_y+5);
	}
}


void Robot::MoveBody()
{
    
    if(servo_data_x.current_angle > 20)
    {
        motors.moveRight();
    }
    else if (servo_data_x.current_angle < -20)
    {
        motors.moveLeft();
    }
    else
    {
        motors.moveStop();
    }
    
}

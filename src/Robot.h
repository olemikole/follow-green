#pragma once
#include "Motors.h"
#include "Servos.h"

class Robot
{
public:
    Robot();
    void MoveCamera(const int x, const int y, int camera_width=1280, int camera_height=720);
    void MoveBody();
private:
    Servos servos;
    Motors motors;
    int motor_direction;
    ServoData servo_data_x;
    ServoData servo_data_y;
    int not_found_count;
};